compile:
	guild compile lol -o lol.go


install:
	cp ./lol.go ~/.local/bin/
	echo "Please make sure you have $HOME/local/bin in your PATH"

clean:
	rm lol.go || true

uninstall:
	rm ~/.local/bin/lol || true
