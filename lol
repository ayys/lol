#!/usr/bin/env guile
!#
;; Copyright (C) 2016 Ayush Jha <jha.ayush@mail.com>
;;
;; LOL(Lots Of Love) - Overflow your loved one's message box with lots of cute
;; emojis and messages
;;
;; LOL is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; LOL is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with LOL.  If not, see <http://www.gnu.org/licenses/>.

;; LOL - Lots Of Love is a little script that prints cute **long** messages
;; so I don't have to.
;; ** Flood your girlfriend's message box with so much love that she will HAVE
;; to stop blabering.**
;; LOL - Lots Of Love <3
;; AUTHOR: Ayush Jha

(define *messages* '((😍)
		     (😍)
		     (😘)
                     (❤)
                     (❤)
                     (😃)
                     (😉)
		     (suruchi)
		     (suru)
		     (sursuriya)
		     (chudel)
		     (raani)
		     (mithu)
		     (hammarrrr dullaaaru)
		     (guddu)
		     (pyaaru)
		     (dil ke tukra)
		     (mohini)
		     (yau)
		     (suruchiya)
		     (dulaaru chauri)
		     (hamar bahsal ladki)
		     (guttu muttu)
		     (chunnu munnu)
		     (jhun jhun)
		     (mun munya)
		     (khurluchiya)
		     (puchuuuu)
		     (maharaani)
		     (malkaain)
		     (dil ke dharkan)
		     (premika)
		     (hamar aashik)
		     (hamar begam)
                     (☺)
                     (😘)
                     (😃)
		     (I love you <3)
		     (I mean it!)
                     (re chauri )
                     (chudel )
                     (paapi)
                     (go to hell i hate u fake habu)
                     (hamar pyaaaruuuuu mithuuuu raaaaniiiiiii)
                     (ham ahike chi jaanu)
                     (pyaaru)
                     (baby )
                     (soni moni)
                     (jaaanuuuu)
                     (jaan)
                     (hamar jigar ke tukra)
                     (hamar cutie cutie pyaaru suruchi)
                     (suru)
                     (hamar raani)
                     (yau kanya)
                     (I LOVE YOU SOOO MUCHHHHHH)
                     (ETEE besi besi besi besi besi maya)
                     (MUUUAAAAAAAHHHH)
                     (hamar pyaaruuuu dulaaruu suruchi)
		     (chooni mooooniiiiiiiiiiii)
		     (aaaaau puchi puchi ka dai chi)
		     (muh ke lereee ler sa bhair dai chi)
		     (eteee besi besi besi besi besiiii mayaaaa)
		     (jaaanuuuuuuuuuuuu)
		     (aaau naaaaaaaaaa)
		     (I miss you soo much so much so so so so so so sooooo much)
		     (💋)))


(define *default-number-of-messages* 10) ;; arbitrary number 10
(define *message-delimeter* " ") ;; delimeter between two consicutive messages

(define (randomly-choose-between option-one option-two)
  ;; randomly choose between two options - one or two
  (if (zero? (random 2 (random-state-from-platform)))
      option-one
      option-two))


(define* (message->string messages #:optional delimeter)
  ;; message is a list of symbols
  ;; convert all the symbols in the list to a string
  (if (equal? delimeter #f)
      (set! delimeter ""))
  (string-join (map symbol->string messages) delimeter))


(define (generate-random-message)
  ;; generate one random message from the list of messages
  ;; given by *messages*
  (let ((index
	 (random (length *messages*) (random-state-from-platform))))
    (message->string (list-ref *messages* index) " ")))


(define (generate-kissie)
  ;; generates a mmmmuuuuuuuuuaaaaaaaaaaah
  ;; with 'u' and 'a' of random length and
  ;; one m and h at the start and end of the
  ;; list
  (let ((output '()))
    (define (add source to-add)
      ;; appends to-add to the list source
      (append source (list to-add)))
    (define (add-n source to-add n)
      ;; repeats the add function n times
      (if (< n 1)
	  source
	  (add-n (add source to-add) to-add (1- n))))
    (message->string (add (add-n (add-n (add output 'm) 'u 5) 'a 5) 'h))))


(define (generate-message number)
  ;; generate the message of size number
  ;; the messages are printed to standard output
  ;; as soon as they are generated
  (if (> number 1)
      (let ((message ((randomly-choose-between
		       generate-kissie
		       generate-random-message))))
	(display message)
	(display *message-delimeter*)
	(generate-message (1- number)))
      (newline)))


(define (get-argument-number-of-messages)
  ;; get the number of messages to print
  ;; from the commandline arguments
  ;; default is *default-number-of-messages*
  (let ((args (command-line)))
    (if (< (length args) 2)
	*default-number-of-messages*
	(string->number (cadr args)))))


(generate-message (get-argument-number-of-messages))
