# lol
LOL - Lots Of Love is a little script that prints cute **long** messages

## USAGE
```
./lol [number-of-messages]
```

## DEPENDENCIES

* Guile Scheme > 2.0.0

## SAMPLE OUTPUT
```
I love you <3 muuuuuaaaaah I love you <3 I love you <3 <3 muuuuuaaaaah
😍 😍 muuuuuaaaaah muuuuuaaaaah 😍   muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah
😘 I mean it! muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah
muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah  😍  I love you <3 ❤ <3
muuuuuaaaaah muuuuuaaaaah 😘 muuuuuaaaaah 😘 <3 muuuuuaaaaah muuuuuaaaaah 💋 <3
😍 muuuuuaaaaah  muuuuuaaaaah 💋 😍 muuuuuaaaaah  muuuuuaaaaah 😘 muuuuuaaaaah
muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah ❤ muuuuuaaaaah  muuuuuaaaaah
muuuuuaaaaah I mean it! I mean it! 💋 😍 muuuuuaaaaah 😍 muuuuuaaaaah
💋 muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah 😍 muuuuuaaaaah muuuuuaaaaah
muuuuuaaaaah 😍 muuuuuaaaaah muuuuuaaaaah 😍 💋 I mean it! muuuuuaaaaah
muuuuuaaaaah <3 muuuuuaaaaah <3 😍 muuuuuaaaaah muuuuuaaaaah muuuuuaaaaah <3 <3
muuuuuaaaaah I mean it! 😘 😍
```
