#!/usr/bin/env bash

sudo apt-get install guile-2.2 git make
git clone https://gitlab.com/ayys/lol /tmp/lol
cd /tmp/lol
make
sudo make install
